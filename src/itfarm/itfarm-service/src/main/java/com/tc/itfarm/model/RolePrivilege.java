package com.tc.itfarm.model;

import java.io.Serializable;
import java.util.Date;

public class RolePrivilege implements Serializable {
    private Integer recordId;

    private Integer roleId;

    private Integer privilegeId;

    private String privilegeCode;

    private Date createTime;

    private static final long serialVersionUID = 1L;

    public RolePrivilege(Integer recordId, Integer roleId, Integer privilegeId, String privilegeCode, Date createTime) {
        this.recordId = recordId;
        this.roleId = roleId;
        this.privilegeId = privilegeId;
        this.privilegeCode = privilegeCode;
        this.createTime = createTime;
    }

    public RolePrivilege() {
        super();
    }

    public Integer getRecordId() {
        return recordId;
    }

    public void setRecordId(Integer recordId) {
        this.recordId = recordId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getPrivilegeId() {
        return privilegeId;
    }

    public void setPrivilegeId(Integer privilegeId) {
        this.privilegeId = privilegeId;
    }

    public String getPrivilegeCode() {
        return privilegeCode;
    }

    public void setPrivilegeCode(String privilegeCode) {
        this.privilegeCode = privilegeCode == null ? null : privilegeCode.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}