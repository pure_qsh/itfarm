<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ include file="/WEB-INF/layouts/basic.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
</head>
<body>
<div id="dcWrap">
    <jsp:include page="../header.jsp"></jsp:include>
    <div id="dcMain">
        <!-- 当前位置 -->
        <div id="urHere">ITFARM 管理中心<b>></b><strong>自定义菜单栏</strong> </div>   <div class="mainBox" style="height:auto!important;height:550px;min-height:550px;">
        <h3><a href="${ctx}/menu/list.do" class="actionBtn">返回列表</a>自定义菜单栏</h3>
        <script type="text/javascript">
            $(function(){ $(".idTabs").idTabs(); });
        </script>
        <div class="idTabs">
            <ul class="tab">
                <li><a href="#nav_add">添加站内导航</a></li>
                <li><a href="#nav_defined">添加自定义链接</a></li>
            </ul>
            <div class="items">
                <div id="nav_add">
                    <form action="${ctx}/menu/save.do" method="post">
                        <input type="hidden" name="recordId" value="${menu.recordId}">
                        <table width="100%" border="0" cellpadding="5" cellspacing="1" class="tableBasic">
                            <tr>
                                <td width="80" height="35" align="right">菜单名称</td>
                                <td>
                                    <input type="text" name="name" value="${menu.name}" size="40" class="inpMain" />
                                </td>
                            </tr>
                            <tr>
                                <td width="80" height="35" align="right">菜单url</td>
                                <td>
                                    <input type="text" name="url" value="${menu.url}" size="40" class="inpMain" />
                                </td>
                            </tr>
                            <tr>
                                <td height="35" align="right">位置</td>
                                <td>
                                    <label for="type">
                                        <input type="radio" id="type" name="type" value="middle" checked="true">
                                        主导航</label>
                                </td>
                            </tr>
                            <tr>
                                <td height="35" align="right">关联文章</td>
                                <td>
                                    <c:forEach items="${articles}" var="item" varStatus="vs">
                                        <label for="aaa${vs.count}"><input id="aaa${vs.count}" align="left" type="checkbox" name="ids" <c:if test="${fn:contains(ids, item.recordId)}">checked="checked"</c:if> value="${item.recordId}">${item.title}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                        <c:if test="${vs.count % 3 == 0}">
                                            <br><br>
                                        </c:if>
                                    </c:forEach>
                                </td>
                            </tr>
                            <tr>
                                <td height="35" align="right">上级分类</td>
                                <td id="parent">
                                    <select name="parent" onchange="changeSelect(this)">
                                        <option value="">无</option>
                                        <c:forEach items="${menus}" var="item">
                                        <option value="${item.recordId}" <c:if test="${item.recordId == menu.parent}">selected="selected"</c:if>>${item.name}</option>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td height="35" align="right">排序</td>
                                <td>
                                    <select id="order" name="orderNo" <c:if test="${menu != null && menu.parent != null}">disabled="disabled"</c:if>>
                                        <c:if test="${menu != null}">
                                        <option value="">${menu.orderNo}</option>
                                        </c:if>
                                         <c:forEach begin="${maxOrder}" end="${maxOrder + 60}" step="10" varStatus="vs">
                                                <option value="${vs.index}">${vs.index}</option>
                                         </c:forEach>
                                    </select>
                                    <a id="meg" style="color: red"></a>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>
                                    <input name="submit" class="btn" type="submit" value="提交" />
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
                <script type="text/javascript">
                    function changeSelect(e) {
                        if (e.value != "") {
                            /*alert('不可选择');
                            var option = $("<option>").val("").text("不可选择");
                            $("#order").append(option);
                            $("#order").find("option[val='']").attr("selected",true);*/
                            $("#order").attr("disabled", "disabled");
                            $("#meg").text("子分类不能选择");
                        } else {
                            $("#meg").text("");
                            $("#order").removeAttr("disabled");
                        }
                    }
                </script>
            </div>
        </div>
    </div>
    </div>
    <div class="clear"></div>
    <jsp:include page="../footer.jsp"></jsp:include>
    <div class="clear"></div> </div>
</body>
</html>