package com.tc.itfarm.service;

import com.tc.itfarm.api.model.Page;
import com.tc.itfarm.api.model.PageList;
import com.tc.itfarm.model.User;

import java.util.Date;

public interface UserService extends BaseService<User> {
	Integer save(User user);
	
	PageList<User> selectUserByPage(String username, String nickname, Date startDate, Date endDate, Page page);
	
	User selectByUsername(String username);

	/**
	 * 根据用户名和密码查询
	 * @param username
	 * @param password
	 * @return
	 */
	User select(String username, String password);

    Integer save(User user, Integer[] roleIds);

}
