package com.tc.itfarm.web.vo;

import com.tc.itfarm.model.Article;
import com.tc.itfarm.model.ext.ArticleEx;

public class ArticleVO {
	
	private Article article;
	
	private String lastDate;
	
	private String authorName;
	
	private String categoryName;

	private Integer commentCount;
	
	private ArticleEx last;
	
	private ArticleEx next;
	
	private Article nextArticle;
	
	private boolean titleImgIsExist;

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public String getLastDate() {
		return lastDate;
	}

	public void setLastDate(String lastDate) {
		this.lastDate = lastDate;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public boolean isTitleImgIsExist() {
		return titleImgIsExist;
	}

	public void setTitleImgIsExist(boolean titleImgIsExist) {
		this.titleImgIsExist = titleImgIsExist;
	}

	public ArticleEx getLast() {
		return last;
	}

	public void setLast(ArticleEx last) {
		this.last = last;
	}

	public ArticleEx getNext() {
		return next;
	}

	public void setNext(ArticleEx next) {
		this.next = next;
	}

	public Article getNextArticle() {
		return nextArticle;
	}

	public void setNextArticle(Article nextArticle) {
		this.nextArticle = nextArticle;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}
}
